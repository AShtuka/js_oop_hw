class HamburgerSize {

    constructor(idName, price, calorie) {
        this.price = price;
        this.calorie = calorie;
        this.idName = idName;
    }

    get price() {
        return this._price;
    }

    set price(value) {
        this._price = value;
    }

    get calorie() {
        return this._calorie;
    }

    set calorie(value) {
        this._calorie = value;
    }

    get idName() {
        return this._idName;
    }

    set idName(value) {
        this._idName = value;
    }
}

class HamburgerTopping {

    constructor(idName, price, calorie) {
        this.price = price;
        this.calorie = calorie;
        this.idName = idName;
    }

    get price() {
        return this._price;
    }

    set price(value) {
        this._price = value;
    }

    get calorie() {
        return this._calorie;
    }

    set calorie(value) {
        this._calorie = value;
    }

    get idName() {
        return this._idName;
    }

    set idName(value) {
        this._idName = value;
    }
}

class HamburgerStuffing {

    constructor(idName, price, calorie) {
        this.price = price;
        this.calorie = calorie;
        this.idName = idName;
    }

    get price() {
        return this._price;
    }

    set price(value) {
        this._price = value;
    }

    get calorie() {
        return this._calorie;
    }

    set calorie(value) {
        this._calorie = value;
    }

    get idName() {
        return this._idName;
    }

    set idName(value) {
        this._idName = value;
    }
}

const hamburgerSizeDataBase = [
    new HamburgerSize("SIZE_SMALL", 50, 20),
    new HamburgerSize("SIZE_LARGE",100, 40)];

const hamburgerStuffingDataBase = [
    new HamburgerStuffing("STUFFING_CHEESE",10, 20),
    new HamburgerStuffing("STUFFING_SALAD",20, 5),
    new HamburgerStuffing("STUFFING_POTATO",15, 10)];

const hamburgerToppingDataBase = [
    new HamburgerTopping("TOPPING_MAYO",20, 5),
    new HamburgerTopping("TOPPING_SPICE",15, 0)];

class HamburgerException{
    constructor(message) {
        this.name = "HamburgerException";
        this.message = message;
    }
    getMessage() {
        return this.message;
    }
}

class Hamburger {

    static SIZE_SMALL = "SIZE_SMALL";
    static SIZE_LARGE = "SIZE_LARGE";
    static STUFFING_CHEESE = "STUFFING_CHEESE";
    static STUFFING_SALAD = "STUFFING_SALAD";
    static STUFFING_POTATO = "STUFFING_POTATO";
    static TOPPING_MAYO = "TOPPING_MAYO";
    static TOPPING_SPICE = "TOPPING_SPICE";

    constructor(size, stuffing) {
        this.hamburgerSize = size;
        this.hamburgerStaffing = stuffing;
        this._hamburgerToppings = [];
    }

    get hamburgerSize() {
        return this._hamburgerSize;
    }

    set hamburgerSize(value) {
        if (!value) {
            throw new HamburgerException("no size given");
        } else if (!hamburgerSizeDataBase.some(item => item._idName === value)) {
            throw new HamburgerException(`invalid size '${value}'`);
        } else {
            this._hamburgerSize = value;
        }
    }

    get hamburgerStaffing() {
        return this._hamburgerStaffing;
    }

    set hamburgerStaffing(value) {
        if (!value) {
            throw new HamburgerException("no stuffing given");
        } else if (!hamburgerStuffingDataBase.some(item => item._idName === value)) {
            throw new HamburgerException(`invalid stuffing '${value}'`);
        } else {
            this._hamburgerStaffing = value;
        }
    }

    addTopping = function (topping) {
        if (!topping || !hamburgerToppingDataBase.some(item => item._idName === topping)) {
            throw new HamburgerException(`invalid topping '${topping}'`);
        } else if (this._hamburgerToppings.includes(topping)) {
            throw new HamburgerException(`duplicate topping '${topping}'`);
        } else {
            this._hamburgerToppings.push(topping);
        }
    }

    removeTopping = function (topping) {
        if (this._hamburgerToppings.length === 0) {
            throw new HamburgerException("you don't have any topping");
        } else if (!topping || !hamburgerToppingDataBase.some(item => item._idName === topping)) {
            throw new HamburgerException(`invalid topping '${topping}'`);
        }

        let index = this._hamburgerToppings.findIndex(function (item) {
            return item === topping;
        })

        if (index === -1) {
            throw new HamburgerException(`you didn't add '${topping}'`);
        } else {
            return this._hamburgerToppings.splice(index, 1);
        }
    }

    getToppings = function () {
        return this._hamburgerToppings;
    }

    getSize = function () {
        return this._hamburgerSize;
    }

    getStuffing = function () {
        return this._hamburgerStaffing;
    }

    calculatePrice = function () {
        return this._hamburgerToppings.reduce(function (accumulator, currentItem) {
            return accumulator + hamburgerToppingDataBase.find(item => item._idName === currentItem)._price;
        }, 0) + hamburgerStuffingDataBase.find(item => item._idName === this._hamburgerStaffing)._price +
                hamburgerSizeDataBase.find(item => item._idName === this._hamburgerSize)._price;
    }

    calculateCalories = function (){
        return this._hamburgerToppings.reduce(function (accumulator, currentItem) {
                return accumulator + hamburgerToppingDataBase.find(item => item._idName === currentItem)._calorie;
            }, 0) + hamburgerStuffingDataBase.find(item => item._idName === this._hamburgerStaffing)._calorie +
                    hamburgerSizeDataBase.find(item => item._idName === this._hamburgerSize)._calorie;
    }

}



try {
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    console.log(hamburger);
    // добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // hamburger.addTopping(Hamburger.TOPPING_MAYO);
    console.log(hamburger.getToppings());;
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
    console.log(hamburger);
    let h = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
    h.addTopping(Hamburger.TOPPING_MAYO);
    h.removeTopping(Hamburger.TOPPING_MAYO);
    console.log(h)
} catch (e) {
    if (e.name === "HamburgerException") {
        console.error("=> " + e.name + ": " + e.getMessage());
    } else {
        throw e;
    }
}

