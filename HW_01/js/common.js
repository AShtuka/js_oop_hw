/**
 * Data base simulate
 */

let sizeDataBase = new Map();
sizeDataBase.set("SIZE_SMALL", {price: 50, calorie: 20});
sizeDataBase.set("SIZE_LARGE", {price: 100, calorie: 40});

let staffingDataBase = new Map();
staffingDataBase.set("STUFFING_CHEESE", {price: 10, calorie: 20});
staffingDataBase.set("STUFFING_SALAD", {price: 20, calorie: 5});
staffingDataBase.set("STUFFING_POTATO", {price: 15, calorie: 10});

let toppingDataBase = new Map();
toppingDataBase.set("TOPPING_MAYO", {price: 20, calorie: 5});
toppingDataBase.set("TOPPING_SPICE", {price: 15, calorie: 0});


/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    if (!size) {
        throw new HamburgerException("no size given");
    } else if (!size.startsWith("SIZE_")) {
        throw new HamburgerException(`invalid size '${size}'`);
    } else if (!stuffing) {
        throw new HamburgerException("no stuffing given");
    } else if (!stuffing.startsWith("STUFFING_")){
        throw new HamburgerException(`invalid stuffing '${stuffing}'`);
    } else {
        this._hamburgerSize = size;
        this._hamburgerStaffing = stuffing;
        this._hamburgerToppings = [];
    }
}

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = "SIZE_SMALL";
Hamburger.SIZE_LARGE = "SIZE_LARGE";
Hamburger.STUFFING_CHEESE = "STUFFING_CHEESE";
Hamburger.STUFFING_SALAD = "STUFFING_SALAD";
Hamburger.STUFFING_POTATO = "STUFFING_POTATO";
Hamburger.TOPPING_MAYO = "TOPPING_MAYO";
Hamburger.TOPPING_SPICE = "TOPPING_SPICE";

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (!topping || !topping.startsWith("TOPPING_")) {
        throw new HamburgerException(`invalid topping '${topping}'`);
    } else if (this._hamburgerToppings.includes(topping)) {
        throw new HamburgerException(`duplicate topping '${topping}'`);
    } else {
        this._hamburgerToppings.push(topping);
    }
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    if (this._hamburgerToppings.length === 0) {
        throw new HamburgerException("you don't have any topping");
    } else if (!topping || !topping.startsWith("TOPPING_")) {
        throw new HamburgerException(`invalid topping '${topping}'`);
    }

    let index = this._hamburgerToppings.findIndex(function (item) {
        return item === topping;
    })

    if (index === -1) {
        throw new HamburgerException(`you didn't add '${topping}'`);
    } else {
        return this._hamburgerToppings.splice(index, 1);
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    const result = [];
    this._hamburgerToppings.forEach(function (item) {
        result.push(item.name);
    })
    return result;
}

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    return this._hamburgerSize.name;
}

/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.hamburgerStuffing.name;
}

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let addPrice = 0;
    this._hamburgerToppings.forEach(function (item) {
        addPrice += toppingDataBase.get(item).price;
    })
    return addPrice + sizeDataBase.get(this._hamburgerSize).price + staffingDataBase.get(this._hamburgerStaffing).price;
}

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function (){
    let addCalorie = 0;
    this._hamburgerToppings.forEach(function (item) {
        addCalorie += toppingDataBase.get(item).calorie;
    })
    return addCalorie + sizeDataBase.get(this._hamburgerSize).calorie + staffingDataBase.get(this._hamburgerStaffing).calorie;
}

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.message = message;
    this.name = "HamburgerException";
}

try {
    // let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    // console.log(hamburger);
    // // добавка из майонеза
    // hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // console.log(hamburger);
    // // спросим сколько там калорий
    // console.log("Calories: %f", hamburger.calculateCalories());
    // // сколько стоит
    // console.log("Price: %f", hamburger.calculatePrice());
    // // я тут передумал и решил добавить еще приправу
    // hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // // А сколько теперь стоит?
    // console.log("Price with sauce: %f", hamburger.calculatePrice());
    // // Проверить, большой ли гамбургер?
    // console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // // Убрать добавку
    // hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    // console.log("Have %d toppings", hamburger.getToppings().length); // 1
    // console.log(hamburger);
    // let h = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
    // h.addTopping(Hamburger.TOPPING_MAYO);
    // h.removeTopping(Hamburger.TOPPING_MAYO);
    // console.log(h)
} catch (e) {
    if (e.name === "HamburgerException") {
        console.error("=> " + e.name + ": " + e.message );
    } else {
        throw e;
    }
}

